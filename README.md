# Mini Capstone 2 National Park Campsite Reservation CLI

This project was completed at the end of Tech Elevator's second module and was designed to demonstrate our mastery 
of PostgreSQL and Spring JDBC.

*Please see module-2-capstone-requirements.pdf in the ETC folder if you're interested in specific requirements.

