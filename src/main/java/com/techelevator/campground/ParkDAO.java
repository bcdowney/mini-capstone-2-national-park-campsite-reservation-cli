package com.techelevator.campground;

import java.util.List;

public interface ParkDAO {
	
	public List<Park> getAllParks();

}
