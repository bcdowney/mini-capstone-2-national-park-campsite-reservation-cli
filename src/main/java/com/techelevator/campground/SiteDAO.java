package com.techelevator.campground;

import java.time.LocalDate;
import java.util.List;

public interface SiteDAO {
	
	public List<Site> getAllAvailableSites(int campgroundId, LocalDate arrivalDate, LocalDate departureDate);
	
	public List<Integer> getSiteNumberFromSiteList(List<Site> sites);

}
