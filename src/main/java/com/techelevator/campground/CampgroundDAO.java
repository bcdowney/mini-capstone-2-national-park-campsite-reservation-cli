package com.techelevator.campground;

import java.math.BigDecimal;
import java.util.List;

public interface CampgroundDAO {
	
	public List getCampgroundsByParkId(int id);
	
	public List getCampgroundIdsByParkId(int id);
	
	public double getDailyFeeByCampgroundId(int id);
	
}
