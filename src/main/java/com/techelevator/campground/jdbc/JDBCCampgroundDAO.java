package com.techelevator.campground.jdbc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.campground.Campground;
import com.techelevator.campground.CampgroundDAO;

public class JDBCCampgroundDAO implements CampgroundDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCCampgroundDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Campground> getCampgroundsByParkId(int id) {
		List<Campground> campgroundSearchResult = new ArrayList<Campground>();
		String sqlCampgroundSearch = "SELECT * FROM campground WHERE park_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlCampgroundSearch, id);
		while (results.next()) {
			Campground campgroundResults = mapRowToPark(results);
			campgroundSearchResult.add(campgroundResults);
		}
		return campgroundSearchResult;
	}
	
	@Override
	public List getCampgroundIdsByParkId(int id) {
		List<Integer> campgroundIds = new ArrayList<Integer>();
		String sqlCampgroundIdSearch = "SELECT campground_id FROM campground WHERE park_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlCampgroundIdSearch, id);
		while (results.next()) {
			Integer campId = results.getInt("campground_id");
			campgroundIds.add(campId);
		}
		return campgroundIds;
	}
	
	@Override
	public double getDailyFeeByCampgroundId(int id) {
		double fee = 0;
		String sqlCampgroundDailyFee = "SELECT CAST(daily_fee AS DECIMAL) FROM campground WHERE campground_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlCampgroundDailyFee, id);
		if (results.next()) {
			fee = results.getDouble("daily_fee");
		}
		return fee;
		
	}
	
	private Campground mapRowToPark(SqlRowSet result) {
		Campground campground = new Campground();
		campground.setCampgroundId(result.getInt("campground_id"));
		campground.setParkId(result.getInt("park_id"));
		campground.setName(result.getString("name"));
		campground.setOpenFromMM(result.getInt("open_from_mm"));
		campground.setOpenToMM(result.getInt("open_to_mm"));
		campground.setDailyFee(result.getDouble("daily_fee"));
		return campground;
	}

}
