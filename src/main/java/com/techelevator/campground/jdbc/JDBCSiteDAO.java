package com.techelevator.campground.jdbc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.campground.Site;
import com.techelevator.campground.SiteDAO;

public class JDBCSiteDAO implements SiteDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCSiteDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	@Override
	public List<Site> getAllAvailableSites(int campgroundId, LocalDate arrivalDate, LocalDate departureDate) {
		List<Site> siteSearchResult = new ArrayList<Site>();
		String sqlSiteAvailabilitySearch = "SELECT DISTINCT * FROM site s\n" + 
				"LEFT JOIN reservation r ON s.site_id = r.site_id\n" + 
				"WHERE s.campground_id = ?\n" + 
				"AND NOT (?::DATE, ?::DATE) OVERLAPS (r.from_date, r.to_date)\n" + 
				"OR r.reservation_id IS NULL AND s.campground_id = ?\n" + 
				"LIMIT 5";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSiteAvailabilitySearch, campgroundId, arrivalDate, departureDate, campgroundId);
		while(results.next()) {
			Site siteResults = mapRowToSite(results);
			siteSearchResult.add(siteResults);
		}
		return siteSearchResult;
	}
	
	@Override
	public List<Integer> getSiteNumberFromSiteList(List<Site> sites) {
		List<Integer> siteNumbers = new ArrayList<Integer>();
		for (Site site : sites) {
			siteNumbers.add(site.getSiteId());
		}
		return siteNumbers;
	}
	
	private Site mapRowToSite(SqlRowSet results) {
		Site site = new Site();
		site.setSiteId(results.getInt("site_id"));
		site.setCampgroundId(results.getInt("campground_id"));
		site.setSiteNumber(results.getInt("site_number"));
		site.setMaxOccupancy(results.getInt("max_occupancy"));
		site.setAccessible(results.getBoolean("accessible"));
		site.setMaxRVLength(results.getInt("max_rv_length"));
		site.setUtilities(results.getBoolean("utilities"));
		return site;
	}
}