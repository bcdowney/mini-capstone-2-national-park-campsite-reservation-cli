package com.techelevator.campground.jdbc;

import java.time.LocalDate;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.campground.Reservation;
import com.techelevator.campground.ReservationDAO;

public class JDBCReservationDAO implements ReservationDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCReservationDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

	@Override
	public Reservation insertReservation(Reservation newReservation, int siteId) {
		LocalDate currentDate = LocalDate.now();
		String sqlAddReservation = "INSERT INTO reservation (reservation_id, site_id, name, from_date, to_date, create_date) "
								+ "VALUES (?, ?, ? , ?, ?, ?)";
		newReservation.setReservationId(getNextReservationId());
		jdbcTemplate.update(sqlAddReservation, newReservation.getReservationId(), siteId, newReservation.getName(), newReservation.getFromDate(), newReservation.getToDate(), currentDate);
		
		return newReservation;
	}
	
	private int getNextReservationId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('reservation_reservation_id_seq')");
		if(nextIdResult.next()) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("An error occurred while retrieving the ID for the new department.");
		}
	}
	
	private Reservation mapRowToReservation(SqlRowSet results) {
		Reservation reservation = new Reservation();
		reservation.setReservationId(results.getInt("reservation_id"));
		reservation.setSiteId(results.getInt("site_id"));
		reservation.setName(results.getString("name"));
		reservation.setFromDate(results.getDate("from_date").toLocalDate());
		reservation.setToDate(results.getDate("to_date").toLocalDate());
		reservation.setCreateDate(java.time.LocalDate.now());
			
			return reservation;
	}
	

}
