package com.techelevator.campground.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.campground.Park;
import com.techelevator.campground.ParkDAO;

public class JDBCParkDAO implements ParkDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getAllParks() {
		List<Park> parks = new ArrayList<Park>();
		String sqlSelectAllParks = "SELECT * FROM park ORDER BY name ASC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);
		while(results.next()) {
			Park park = mapRowToPark(results);
			parks.add(park);
		}
		return parks;
	}

	private Park mapRowToPark(SqlRowSet result) {
		Park park = new Park();
		park.setParkId(result.getInt("park_id"));
		park.setName(result.getString("name"));
		park.setLocation(result.getString("location"));
		park.setEstablishDate(result.getDate("establish_date").toLocalDate());
		park.setArea(result.getInt("area"));
		park.setVisitors(result.getInt("visitors"));
		park.setDescription(result.getString("description"));
		return park;
	}
}
