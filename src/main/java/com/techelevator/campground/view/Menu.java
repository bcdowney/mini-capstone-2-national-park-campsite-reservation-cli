package com.techelevator.campground.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

	private PrintWriter out;
	private Scanner in;

	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}

	public Object getChoiceFromOptions(Object[] options) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options);
			choice = getChoiceFromUserInput(options);
		}
		return choice;
	}
	
	public String getUserStringEntry(Object[] options) {
		displayMenuOptions(options);
		String userInput = in.nextLine();
		return userInput;
	}
	
	public Object getChoiceFromOptionsGivenIntegerList(Object[] options, List<Integer> validNumbers) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options);
			choice = getChoiceFromUserInputGivenValidNumberList(validNumbers);
		}
		return choice;
	}

	public LocalDate getDateFromOptions(Object[] options) {
		LocalDate choice = null;
		while(choice == null) {
			displayMenuOptions(options);
			choice = (LocalDate) getDateFromUserInput();
		}
		return choice;
	}
	
	private Object getChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		try {
			int selectedOption = Integer.valueOf(userInput);
			if(selectedOption <= options.length) {
				choice = selectedOption; //added to make return integers
				//choice = options[selectedOption - 1]; Re-enable for string choices
			}
		} catch(NumberFormatException e) {
			
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}
	
	private Object getDateFromUserInput(){
		LocalDate choice = null;
		String userInput = in.nextLine();
		try {
			LocalDate selectedOption = LocalDate.parse(userInput, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
			choice = selectedOption;
			if (choice.isBefore(LocalDate.now())) {
				choice = null;
				throw new DateTimeParseException("", userInput, 0);
			}
		}
		catch(DateTimeParseException e) {
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
		
	}
	
	private Object getChoiceFromUserInputGivenValidNumberList(List<Integer> validNumbers) {
		Object choice = null;
		String userInput = in.nextLine();
		try {
			Integer selectedOption = Integer.valueOf(userInput);
			if(validNumbers.contains(selectedOption) || selectedOption == 0) {
				choice = selectedOption; //added to make return integers
				//choice = options[selectedOption - 1]; Re-enable for string choices
			}
		} catch(NumberFormatException e) {
			
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}

	private void displayMenuOptions(Object[] options) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
		}
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}

	public void displaySplitString(String msg, int lineSize) {
        List<String> res = new ArrayList<>();

        Pattern p = Pattern.compile("\\b.{1," + (lineSize-1) + "}\\b\\W?");
        Matcher m = p.matcher(msg);
        
    while(m.find()) {
                System.out.println(m.group().trim());   // Debug
                res.add(m.group());
        }
    }
}
