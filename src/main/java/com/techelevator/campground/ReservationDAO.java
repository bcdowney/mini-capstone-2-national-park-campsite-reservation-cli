package com.techelevator.campground;


public interface ReservationDAO {
	
	public Reservation insertReservation(Reservation newReservation, int siteId);

}
