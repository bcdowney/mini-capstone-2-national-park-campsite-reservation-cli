package com.techelevator;

import java.time.LocalDate;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.campground.Campground;
import com.techelevator.campground.CampgroundDAO;
import com.techelevator.campground.Park;
import com.techelevator.campground.ParkDAO;
import com.techelevator.campground.Reservation;
import com.techelevator.campground.ReservationDAO;
import com.techelevator.campground.Site;
import com.techelevator.campground.SiteDAO;
import com.techelevator.campground.jdbc.JDBCCampgroundDAO;
import com.techelevator.campground.jdbc.JDBCParkDAO;
import com.techelevator.campground.jdbc.JDBCReservationDAO;
import com.techelevator.campground.jdbc.JDBCSiteDAO;
import com.techelevator.campground.view.Menu;

public class CampgroundCLI {
	
	private static final String QUIT_OPTION = "Quit";
	private static final String GO_BACK_OPTION = "Go Back";
	
	private static final String PARK_MENU_OPTION_CAMPGROUNDS = "View Campgrounds";
	//private static final String PARK_MENU_OPTION_RESERVATION = "Search for Reservation"; // Feature not implemented
	private static final String[] PARK_MENU_OPTIONS = new String[] {PARK_MENU_OPTION_CAMPGROUNDS, /*PARK_MENU_OPTION_RESERVATION,*/ GO_BACK_OPTION}; //Park system-wide reservation search not implemented
	
	private static final String CAMPGROUND_MENU_OPTION_RESERVATIONS = "Search for Available Reservation";
	private static final String[] CAMPGROUND_MENU_OPTIONS= new String[] {CAMPGROUND_MENU_OPTION_RESERVATIONS, GO_BACK_OPTION};
	
	private static final String[]CAMPGROUND_RESERVATION_MENU_CAMPGROUND = new String[] {"Which campground (enter 0 to cancel)?"};
	private static final String[] CAMPGROUND_RESERVATION_MENU_ARRIVAL_DATE = new String[] {"What is the arrival date? (mm/dd/yyyy)"};
	private static final String[] CAMPGROUND_RESERVATION_MENU_DEPARTURE_DATE = new String[] {"What is the departure date? (mm/dd/yyyy)"};
	
	private static final String[] SITE_RESERVATION_MENU_SITE = new String[] {"Which site should be reserved (enter 0 to cancel)?"};
	private static final String[] SITE_RESERVATION_MENU_NAME = new String[] {"What name should the reservation be made under?"};

	private Menu menu;
	private CampgroundDAO campgroundDAO;
	private ParkDAO parkDAO;
	private ReservationDAO reservationDAO;
	private SiteDAO siteDAO;
	
	public static void main(String[] args) {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		CampgroundCLI application = new CampgroundCLI(dataSource);
		application.run();
	}

	public CampgroundCLI(DataSource dataSource) {
		this.menu = new Menu(System.in, System.out);
		parkDAO = new JDBCParkDAO(dataSource);
		campgroundDAO = new JDBCCampgroundDAO(dataSource);
		siteDAO = new JDBCSiteDAO(dataSource);
		reservationDAO = new JDBCReservationDAO(dataSource);
	}
	
	public void run() {
		displayProgramBanner();
		List<Park> parkList = parkDAO.getAllParks();
		String[] MAIN_MENU_OPTIONS = generateMainMenu(parkList);
		int mainMenuChoice = (Integer)menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);
		handleIfUserChoosesQuit(mainMenuChoice, MAIN_MENU_OPTIONS);
		Park chosenPark = handleParkInformationForChosenPark(mainMenuChoice, parkList);
		int chosenParkId = chosenPark.getParkId();
		
		listCampgroundsByParkId(chosenParkId, PARK_MENU_OPTIONS);
		listCampgroundsByParkId(chosenParkId, CAMPGROUND_MENU_OPTIONS);
		
		int reserveCampgroundChoice = (Integer)menu.getChoiceFromOptionsGivenIntegerList(CAMPGROUND_RESERVATION_MENU_CAMPGROUND, campgroundDAO.getCampgroundIdsByParkId(chosenParkId));
		handleIfUserChoosesGoBack(reserveCampgroundChoice);
		LocalDate reservationArrivalDate = (LocalDate)menu.getDateFromOptions(CAMPGROUND_RESERVATION_MENU_ARRIVAL_DATE);
		LocalDate reservationDepartureDate = (LocalDate)menu.getDateFromOptions(CAMPGROUND_RESERVATION_MENU_DEPARTURE_DATE);
		List<Site> availableSites = listAvalableSitesByCampgroundId(reserveCampgroundChoice, reservationArrivalDate, reservationDepartureDate);
		handleMakeReservation(availableSites, reservationArrivalDate, reservationDepartureDate);
		}
	
	private String[] generateMainMenu(List<Park> listOfParks) {
		String[] MAIN_MENU_OPTIONS = new String[listOfParks.size() + 1];
		for (int i = 0; i < listOfParks.size(); i++) {
			Park park = new Park();
			park = (Park) listOfParks.get(i);
			MAIN_MENU_OPTIONS[i] = park.getName();
		}
		MAIN_MENU_OPTIONS[listOfParks.size()] = QUIT_OPTION;
		System.out.println("Select a Park for Further Details");
		return MAIN_MENU_OPTIONS;
	}
	
	private Park handleParkInformationForChosenPark(int mainMenuChoice, List<Park> parkList) {
		Park chosenPark = parkList.get(mainMenuChoice - 1);
		System.out.println();
		System.out.println(chosenPark.getName() + " National Park");
		System.out.println();
		System.out.printf("%-17s" + chosenPark.getLocation(), "Location:");
		System.out.println();
		System.out.printf("%-17s" + chosenPark.getEstablishDate(), "Established:");
		System.out.println();
		System.out.printf("%-17s" + chosenPark.getArea() + " sq km", "Area:");
		System.out.println();
		System.out.printf("%-17s" + chosenPark.getVisitors(), "Annual Visitors:");
		System.out.println();
		System.out.println();
		menu.displaySplitString(chosenPark.getDescription(), 80); //Uses
		System.out.println();
		return chosenPark;
	}
	
	private List<Campground> listCampgroundsByParkId(int ParkId, String[] menuOptions) {
		List<Campground> campgroundList= campgroundDAO.getCampgroundsByParkId(ParkId);
		int choice = (Integer)menu.getChoiceFromOptions(menuOptions);
		if (choice == 1) {
			System.out.println();
			System.out.printf("%-7s" + "%-33s" + "%-10s" + "%-10s" + "%-9s", " ", "Name", "Open", "Close", "Daily Fee");
			System.out.println("");
			for (Campground campground : campgroundList) {
				System.out.print("#" + campground.getCampgroundId());
				System.out.printf("%-5s" + "%-33s" + "%-10s" + "%-10s" + "$" + "%3.2f", " ", campground.getName(), campground.getOpenFromMM(), campground.getOpenToMM(), campground.getDailyFee());
				System.out.println("");
				} 
			} else { 
				handleIfUserChoosesGoBack(choice, menuOptions);
		}
		System.out.println();
		return campgroundList;
	}
	
	private List<Site> listAvalableSitesByCampgroundId(int campgroundChoice, LocalDate arrivalDate, LocalDate departureDate) {
		List<Site> availableSites = siteDAO.getAllAvailableSites(campgroundChoice, arrivalDate, departureDate);
		long totalDays = departureDate.toEpochDay() - arrivalDate.toEpochDay();
		if (availableSites.isEmpty()) {
			System.out.println("No sites meet your criteria. Please enter an alternate date range."); //TODO Change?
			arrivalDate = (LocalDate)menu.getDateFromOptions(CAMPGROUND_RESERVATION_MENU_ARRIVAL_DATE);
			departureDate = (LocalDate)menu.getDateFromOptions(CAMPGROUND_RESERVATION_MENU_DEPARTURE_DATE);
			listAvalableSitesByCampgroundId(campgroundChoice, arrivalDate, departureDate);
		} else {
			System.out.println();
			System.out.printf("%-15s" + "%-15s" + "%-15s" + "%-17s" + "%-15s" + "%-15s", "Site No.", "Max Occup.", "Accessible?", "Max RV Length", "Utility", "Cost");
			System.out.println();
			for (Site site : availableSites) {
				System.out.printf("%-15s" + "%-15s" + "%-15s" + "%-17s" + "%-15s" + "$" + "%6.2f", site.getSiteId(), site.getMaxOccupancy(), site.isAccessible(), site.getMaxRVLength(), site.isUtilities(), (totalDays * campgroundDAO.getDailyFeeByCampgroundId(campgroundChoice)));
				System.out.println();
			} 
		}
		return availableSites;
	}
	
	private void handleMakeReservation(List<Site> availableSites, LocalDate startDate, LocalDate endDate) {
		int siteNumberToReserve = (int)menu.getChoiceFromOptionsGivenIntegerList(SITE_RESERVATION_MENU_SITE, siteDAO.getSiteNumberFromSiteList(availableSites));
		handleIfUserChoosesGoBack(siteNumberToReserve);
		String reservationName = menu.getUserStringEntry(SITE_RESERVATION_MENU_NAME);
		Reservation reservation = new Reservation();
		reservation.setName(reservationName);
		reservation.setFromDate(startDate);
		reservation.setToDate(endDate);
		reservationDAO.insertReservation(reservation, siteNumberToReserve);
		System.out.println("The reservation has been made and the confirmation id is: " + reservation.getReservationId());
	}
	
	private void handleIfUserChoosesGoBack(int choice) {
		if (choice == 0) {
			System.out.println();
			run();
		}
	}
	
	private void handleIfUserChoosesGoBack(int choice, String[] menu) {
		if (menu.length == choice) {
			System.out.println();
			run();
		}
	}
	
	private void handleIfUserChoosesQuit(int choice, String[] MAIN_MENU_OPTIONS) {
		if (MAIN_MENU_OPTIONS.length == choice) {
			System.exit(0);
		}
	}
	
	private void displayProgramBanner() {
		System.out.println("***********************************************");
		System.out.println("** NATIONAL PARK CAMPSITE RESERVATION SYSTEM **");
		System.out.println("***********************************************");
	}
}
