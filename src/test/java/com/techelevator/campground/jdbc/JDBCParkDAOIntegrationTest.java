package com.techelevator.campground.jdbc;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.campground.Park;

public class JDBCParkDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCParkDAO parkDAO;
	
	@Before
	public void setup() {
		parkDAO = new JDBCParkDAO(super.getDataSource());
	}
	
	@Test
	public void get_parks_returns_parks() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String insertSQL = "INSERT INTO park VALUES (default, 'TEST_PARK', 'Maine', '1919-02-26', 47389, 2563129, 'TEST')";
		jdbcTemplate.update(insertSQL);
		List<Park> parks = parkDAO.getAllParks();
		Assert.assertTrue(parks.size() > 0);
	}
	
}
