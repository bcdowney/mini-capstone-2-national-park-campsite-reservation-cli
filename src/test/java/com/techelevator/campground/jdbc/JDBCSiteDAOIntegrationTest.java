package com.techelevator.campground.jdbc;

import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.campground.Reservation;
import com.techelevator.campground.Site;

public class JDBCSiteDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCSiteDAO siteDAO;
	
	@Before
	public void setup() {
		siteDAO = new JDBCSiteDAO(super.getDataSource());
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertPark = "INSERT INTO park VALUES (999, 'TEST_PARK', 'Maine', '1919-02-26', 47389, 2563129, 'TEST')";
		jdbcTemplate.update(sqlInsertPark);
		String sqlInsertCampground = "INSERT INTO campground VALUES (5000, 999, 'TEST', '03', '04', '$1.00')";
		jdbcTemplate.update(sqlInsertCampground);
	}

	@Test
	public void verify_search_for_available_sites_returns_available_sites() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertSite = "INSERT INTO site VALUES (10001, 5000, 1827, 6, false, 0, false)";
		jdbcTemplate.update(sqlInsertSite);
		List<Site> sites = siteDAO.getAllAvailableSites(5000,(LocalDate.of(1982, 11, 01)), (LocalDate.of(1982, 11, 15)));
		Assert.assertTrue(sites.size() > 0);
	}
	
	@Test
	public void verify_search_for_available_sites_does_not_return_unavailable_sites() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertSite = "INSERT INTO site VALUES (10001, 5000, 1827, 6, false, 0, false)";
		jdbcTemplate.update(sqlInsertSite);
		String sqlInsertReservation = "INSERT INTO reservation VALUES (12001, 10001, 'Smith Family', '1982-11-02', '1982-11-05', '1982-11-01')";
		jdbcTemplate.update(sqlInsertReservation);
		List<Site> sites = siteDAO.getAllAvailableSites(5000,(LocalDate.of(1982, 11, 01)), (LocalDate.of(1982, 11, 15)));
		Assert.assertFalse(sites.size() > 0);
	}
	
	@Test
	public void make_sure_get_site_numbers_from_site_list_returns_numbers() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertSite = "INSERT INTO site VALUES (10001, 5000, 1827, 6, false, 0, false)";
		jdbcTemplate.update(sqlInsertSite);
		String sqlInsertReservation = "INSERT INTO reservation VALUES (12001, 10001, 'Smith Family', '1982-11-02', '1982-11-05', '1982-11-01')";
		jdbcTemplate.update(sqlInsertReservation);
		List<Site> sites = siteDAO.getAllAvailableSites(5000,(LocalDate.of(1982, 11, 01)), (LocalDate.of(1982, 11, 15)));
		List<Integer> results = siteDAO.getSiteNumberFromSiteList(sites);
		Assert.assertFalse(results.size() > 0);
	}
}
