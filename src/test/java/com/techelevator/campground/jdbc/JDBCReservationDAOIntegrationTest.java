package com.techelevator.campground.jdbc;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.campground.Reservation;
import com.techelevator.campground.ReservationDAO;

public class JDBCReservationDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCReservationDAO reservationDAO;
	
	@Before
	public void setup() {
		reservationDAO = new JDBCReservationDAO(super.getDataSource());
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertSite = "INSERT INTO site VALUES (999, 4, 1015, 6, 'FALSE', 40, 'TRUE')";
		jdbcTemplate.update(sqlInsertSite);
	}
	
	@Test
	public void verify_can_insert_reservation() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		reservationDAO.insertReservation(makeTestReservation(), 999);
		String sqlReservationSearch = "SELECT * FROM reservation WHERE name = 'TEST'";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlReservationSearch);
		Assert.assertTrue(results.next());
	}
	
	private Reservation makeTestReservation() {
		Reservation reservation = new Reservation();
		reservation.setReservationId(10001);
		reservation.setSiteId(999);
		reservation.setName("TEST");
		reservation.setFromDate(LocalDate.of(2011, 01, 21));
		reservation.setToDate(LocalDate.of(2011, 01, 26));
		reservation.setCreateDate(LocalDate.now());
		return reservation;
	}
	
}
