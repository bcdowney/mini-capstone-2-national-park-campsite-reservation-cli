package com.techelevator.campground.jdbc;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.campground.Campground;

public class JDBCCampgroundDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCCampgroundDAO campgroundDAO;
	
	@Before
	public void setup() {
		campgroundDAO = new JDBCCampgroundDAO(super.getDataSource());
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlInsertPark = "INSERT INTO park VALUES (999, 'TEST_PARK', 'Maine', '1919-02-26', 47389, 2563129, 'TEST')";
		jdbcTemplate.update(sqlInsertPark);
	}
	
	@Test
	public void get_campground_park_id_returns_campground() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String insertSQL = "INSERT INTO campground VALUES (5000, 999, 'TEST', '03', '04', '$1.00')";
		jdbcTemplate.update(insertSQL);
		List<Campground> campgrounds = campgroundDAO.getCampgroundsByParkId(999);
		Assert.assertTrue(campgrounds.size() > 0);
	}
	
	@Test
	public void get_campground_id_by_park_returns_id() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String insertSQL = "INSERT INTO campground VALUES (5000, 999, 'TEST', '03', '04', '$1.00')";
		jdbcTemplate.update(insertSQL);
		List<Campground> campgrounds = campgroundDAO.getCampgroundIdsByParkId(999);
		Assert.assertTrue(campgrounds.size() > 0);
	}
	
	@Test
	public void get_campground_daily_cost_returns_a_cost() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String insertSQL = "INSERT INTO campground VALUES (5000, 999, 'TEST', '03', '04', '$1.00')";
		jdbcTemplate.update(insertSQL);
		Assert.assertEquals(1.0, campgroundDAO.getDailyFeeByCampgroundId(5000), .1);
	}
}
